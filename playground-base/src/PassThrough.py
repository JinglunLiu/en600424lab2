'''
Create on Oct 10, 2016
@author: Jinglun Liu
'''
#!/usr/bin/env python
#!/usr/bin/python
# -*- coding: utf-8 -*-
from twisted.internet.protocol import Protocol, Factory
from zope.interface.declarations import implements
from twisted.internet.interfaces import ITransport, IStreamServerEndpoint
from playground.network.message.ProtoBuilder import MessageDefinition
from playground.network.message.StandardMessageSpecifiers import STRING,UINT4,OPTIONAL,DEFAULT_VALUE,\
    LIST,BOOL1
from playground.network.common.Protocol import StackingTransport,\
    StackingProtocolMixin, StackingFactoryMixin
import random
import Crypto
from Crypto.Hash import SHA256
from Crypto.Cipher import AES
from Crypto.Signature import PKCS1_v1_5
from Crypto.PublicKey import RSA
from playground.crypto import X509Certificate
from Crypto.Cipher.PKCS1_OAEP import PKCS1OAEP_Cipher
from playground.error import GetErrorReporter

from playground.network.common.statemachine import StateMachine as FSM
from playground.network.common.statemachine import StateMachineError
from playground.network.common.Packet import Packet, PacketStorage, IterateMessages

from playground.network.message.ProtoBuilder import MessageDefinition
from playground.network.message.definitions.playground.base import GateRegistered
from playground.network.message.definitions.playground.base import Gate2GateMessage
from playground.network.message.definitions.playground.base import RegisterGate

from playground.playgroundlog import packetTrace

from ripClient import RipClientProtocol
from ripServer import RipServerProtocol



class RipMessage(MessageDefinition):
    PLAYGROUND_IDENTIFIER = "PassThrough"
    MESSAGE_VERSION = "1.1"
    BODY = [
         ("sequence_number", UINT4, OPTIONAL),
         ("acknowledgement_number", UINT4, OPTIONAL),
         ("signature", STRING, DEFAULT_VALUE("")),
         ("certificate", LIST(STRING), OPTIONAL),
         ("sessionID", STRING,OPTIONAL),
         ("acknowledgement_flag", BOOL1, DEFAULT_VALUE(False)),
         ("close_flag", BOOL1, DEFAULT_VALUE(False)),
         ("sequence_number_notification_flag", BOOL1,
          DEFAULT_VALUE(False)),
         ("reset_flag", BOOL1, DEFAULT_VALUE(False)),
         ("data", STRING, DEFAULT_VALUE("")),
         ("OPTIONS", LIST(STRING), OPTIONAL)
         ]
'''
handshake: exchange certificates, validates them, use the certificate data to create
session ID's and other session elements.
transmission:exchange data. Ensure all flags are set correctly.
'''
class RipTransport(StackingTransport):
    def __init__(self, lowerTransport):
        StackingTransport.__init__(self, lowerTransport)
    #signature is the hash of entire msg
    def write(self, data):
        ripmessage = RipMessage()
        ripmessage.data = data
        #print "transport from", self.getHost(), "to", self.getPeer()
        self.lowerTransport().write(ripmessage.__serialize__())

    def rip_write(self,ripmessage):
        #ripmessage = RipMessage()
        self.lowerTransport().write(ripmessage.__serialize__())

class RipCrypto():
    def load_cert(self, cert_to_load):
        with open(cert_to_load) as f:
            certBytes = f.read()
        print 'load cert:', cert_to_load
        return certBytes

    def create_signer(self, raw_key):
        with open(raw_key) as f:
            rawKey = f.read()
        rsaKey =  RSA.importKey(rawKey)
        rsaSigner = PKCS1_v1_5.new(rsaKey)
        print "create signer"
        return rsaSigner

    def hash_data(self, data, raw_key):
        #crypto = RipCrypto()
        hasher = SHA256.new()
        hasher.update(data)
        rsaSigner = self.create_signer(raw_key)
        signatureBytes = rsaSigner.sign(hasher)
        print "hash_data success"
        return signatureBytes

    def verify_signature(self, ripmessage, raw_cert):
        print 'enter the verify_signiture function'
        #use cert to verify a signature
        signature = ripmessage.signature
        ripmessage.signature = ""
        data = ripmessage.__serialize__()
        cert = X509Certificate.loadPEM(self.load_cert(raw_cert))
        peerPublicKeyBlob = cert.getPublicKeyBlob()
        peerPublicKey = RSA.importKey(peerPublicKeyBlob)
        rsaVerifier = PKCS1_v1_5.new(peerPublicKey)
        hasher = SHA256.new()
        hasher.update(data)
        #print 'signature:', signature
        #print 'hasher:', hasher
        result = rsaVerifier.verify(hasher, signature)
        #print 'verify signature'
        print 'result of verify_signature:', result
        return result


    def verify_cert_chain(self, ripmessage):
        cert = X509Certificate.loadPEM(ripmessage.certificate[1])
        #print 'cert:', cert
        rootCert = X509Certificate.loadPEM(ripmessage.certificate[2])
        #print 'rootcert:', rootCert
        if (cert.getIssuer() != rootCert.getSubject()):
            print 'error here'
            return False
        # now let's check the signature
        rootPkBytes = rootCert.getPublicKeyBlob()
        # use rootPkBytes to get a verifiying RSA key
        rootPk = RSA.importKey(rootPkBytes)
        rootVerifier = PKCS1_v1_5.new(rootPk)
        bytesToVerify = cert.getPemEncodedCertWithoutSignatureBlob()
        hasher = SHA256.new()
        hasher.update(bytesToVerify)
        if not rootVerifier.verify(hasher, cert.getSignatureBlob()):
            print 'verify cert_chain: false'
            return False
        print 'verify cert_chain: true'
        return True




class clientFactory(StackingFactoryMixin, Factory):
    protocol = RipClientProtocol

class ServerFactory(StackingFactoryMixin, Factory):
    protocol = RipServerProtocol

#client
ConnectFactory = clientFactory
#server
ListenFactory = ServerFactory
