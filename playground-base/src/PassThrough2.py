'''
Create on Oct 10, 2016
@author: Jinglun Liu
'''
#!/usr/bin/env python
#!/usr/bin/python
# -*- coding: utf-8 -*-
import CertFactory
from twisted.internet.protocol import Protocol, Factory
from zope.interface.declarations import implements
from twisted.internet.interfaces import ITransport, IStreamServerEndpoint
from playground.network.message.ProtoBuilder import MessageDefinition
from playground.network.message.StandardMessageSpecifiers import STRING,UINT4,OPTIONAL,DEFAULT_VALUE,\
    LIST,BOOL1
from playground.network.common.Protocol import StackingTransport,\
    StackingProtocolMixin, StackingFactoryMixin
import random
import Crypto
from Crypto.Hash import SHA256
from Crypto.Cipher import AES
from Crypto.Signature import PKCS1_v1_5
from Crypto.PublicKey import RSA
from playground.crypto import X509Certificate


from playground.network.message.ProtoBuilder import MessageDefinition

from playground.network.common.Protocol import MessageStorage

class RipMessage(MessageDefinition):
    PLAYGROUND_IDENTIFIER = "RIP.RIPMessageID"
    MESSAGE_VERSION = "1.0"
    BODY = [
         ("sequence_number", UINT4),
         ("acknowledgement_number", UINT4, OPTIONAL),
         ("signature", STRING, DEFAULT_VALUE("")),
         ("certificate", LIST(STRING), OPTIONAL),
         ("sessionID", STRING),
         ("acknowledgement_flag", BOOL1, DEFAULT_VALUE(False)),
         ("close_flag", BOOL1, DEFAULT_VALUE(False)),
         ("sequence_number_notification_flag", BOOL1,
          DEFAULT_VALUE(False)),
         ("reset_flag", BOOL1, DEFAULT_VALUE(False)),
         ("data", STRING, DEFAULT_VALUE("")),
         ("OPTIONS", LIST(STRING), OPTIONAL)
         ]
'''
handshake: exchange certificates, validates them, use the certificate data to create
session ID's and other session elements.
transmission:exchange data. Ensure all flags are set correctly.
'''
class RipTransport(StackingTransport):
    def __init__(self, lowerTransport, protocol):

        StackingTransport.__init__(self, lowerTransport)
        self.protocol = protocol
    def write(self, data):
        #print 'Transport write()', data
        ripmessage = RipMessage()
        ripmessage.data = data
        ripmessage.sequence_number = self.protocol.sequence_number
        ripmessage.sessionID = self.protocol.sessionID
        #print "transport from", self.getHost(), "to", self.getPeer()
        self.lowerTransport().write(ripmessage.__serialize__())


class RipCrypto():
    def load_cert(self, cert_to_load):
        with open(cert_to_load) as f:
            certBytes = f.read()
        #print 'load cert:', cert_to_load
        return certBytes

    def create_signer(self, raw_key):
        # with open(raw_key) as f:
        #     rawKey = f.read()
        # rsaKey =  RSA.importKey(raw_key)
        rsaSigner = PKCS1_v1_5.new(raw_key)
        #print "create signer"
        return rsaSigner

    def hash_data(self, data, raw_key):
        #crypto = RipCrypto()
        hasher = SHA256.new()
        hasher.update(data)
        rsaSigner = self.create_signer(raw_key)
        signatureBytes = rsaSigner.sign(hasher)
        #print "hash_data success"
        return signatureBytes

    def verify_signature(self, ripmessage, raw_cert):
        #print 'enter the verify_signiture function'
        #use cert to verify a signature
        signature = ripmessage.signature
        ripmessage.signature = ""
        data = ripmessage.__serialize__()
        cert = X509Certificate.loadPEM(raw_cert)
        peerPublicKeyBlob = cert.getPublicKeyBlob()
        peerPublicKey = RSA.importKey(peerPublicKeyBlob)
        rsaVerifier = PKCS1_v1_5.new(peerPublicKey)
        hasher = SHA256.new()
        hasher.update(data)
        result = rsaVerifier.verify(hasher, signature)
        return result


    def verify_cert_chain(self, cert1, cert2):
        print 'cert chain verify'
        #print '1', cert1
        #print '2', cert2
        cert = X509Certificate.loadPEM(cert1)
        rootCert = X509Certificate.loadPEM(cert2)
        if (cert.getIssuer() != rootCert.getSubject()):
            print 'error here'
            return False
        #print 'issue'
        # now let's check the signature
        rootPkBytes = rootCert.getPublicKeyBlob()
        # use rootPkBytes to get a verifiying RSA key
        rootPk = RSA.importKey(rootPkBytes)
        rootVerifier = PKCS1_v1_5.new(rootPk)
        bytesToVerify = cert.getPemEncodedCertWithoutSignatureBlob()
        hasher = SHA256.new()
        hasher.update(bytesToVerify)
        if not rootVerifier.verify(hasher, cert.getSignatureBlob()):
            #print 'verify cert_chain: false'
            #print 'No'
            return False
        #print 'verify cert_chain: true'
        return True

class RipClientProtocol(StackingProtocolMixin, Protocol):
    def __init__(self):
        #self.fsm = FSM("MyStateMachine")
        #self.fsm.start("Listening")
        # self.fsm.addState(self.LISTEN, self.FIRST_HANDSHAKE, self.SECOND_HANDSHAKE, \
        #                   self.THIRD_HANDSHAKE, self.DATA_TRANSPORT, self.DATA_RETRANSOIRT)
        #self.buildStateMachine()
        self.address = "20164.1.100.10"
        self.buffer = ""
        self.state = "listen"
        self.message_store = MessageStorage()
    #three time handshake
    def client_first_handshake(self):
        #print 'client 1st state:', self.state
        self.state = 'client_first_handshake'
        print self.state
        #print self.state
        crypto = RipCrypto()
        ripmessage = RipMessage()
        self.sequence_number = 100
        ripmessage.sequence_number = self.sequence_number
        ripmessage.acknowledgement_number = 0
        #sign:the signature of the whole msg
        ripmessage.signature = ""
        #cert:1.Nonce1, 2.sender's certificate, 3.CA's certificate
        ripmessage.certificate = []
        self.sessionID = ''
        ripmessage.sessionID = self.sessionID
        ripmessage.acknowledgement_flag = False
        ripmessage.close_flag = False
        ripmessage.sequence_number_notification_flag = True
        ripmessage.reset_flag = False
        ripmessage.data = ""
        ripmessage.options = []

        #cert
        Nonce1 = random.randint(0, 65536)
        self.Nonce_1 = Nonce1
        #print 'Nonce1', Nonce1
        #print 'Nonce1:', Nonce1
        addr = self.transport.getHost().host
        certs = CertFactory.getCertsForAddr(self.transport.getHost().host)
        certs.insert(0, Nonce1)
        ripmessage.certificate = certs
        self.certsnew = certs[1:]
        #print 'cert:', ripmessage.certificate
        #signature
        signature = crypto.hash_data(ripmessage.__serialize__(), RSA.importKey(CertFactory.getPrivateKeyForAddr(self.transport.getHost().host)))
        ripmessage.signature = signature
        #print 'sign:', ripmessage.signature
        print '1'
        self.transport.write(ripmessage.__serialize__())
        #self.higherTransport.rip_write()
        return True

    def ack_test(self, ripmessage):
        #print ripmessage.acknowledgement_flag
        return ripmessage.acknowledgement_flag

        #pass
    def verify_client_signature(self, ripmessage):
        crypto = RipCrypto()
        #print 'sign:', ripmessage.signature
        #my_cert = CertFactory.getCertsForAddr(self.transport.getHost().host)[0]
        #ca_cert = CertFactory.getCertsForAddr(self.transport.getHost().host)[1]
        peer_cert = self.peerCerts[0]
        if not crypto.verify_signature(ripmessage,peer_cert):
            #print 'func:signature fail'
            return False
        #print 'verify signature: yes'
        return True
    def verify_certificate(self, ripmessage):
        #certificate chain;
        crypto = RipCrypto()
        root_cert = CertFactory.getRootCert()
        my_cert = CertFactory.getCertsForAddr(self.transport.getHost().host)[0]
        ca_cert = CertFactory.getCertsForAddr(self.transport.getHost().host)[1]
        if not crypto.verify_cert_chain(ca_cert, root_cert):
            print 'root cert fail'
            return False
        if not crypto.verify_cert_chain(my_cert, ca_cert):
            print 'no'
            return False
        #print 'verify certificate: yes'
        return True


    def client_second_HandShake(self, ripmessage):
        #print '--------------------------'
        if not self.state == 'client_first_handshake':
            return False
        self.state = 'client_second_handshake'
        print self.state
        self.peerCerts = ripmessage.certificate[2:]

        #print 'client 2 handshake:', ripmessage.acknowledgement_flag
        if not ripmessage.acknowledgement_flag:
            return False
        #print 'ack_flag: true'
        if not self.verify_certificate(ripmessage):
            return False
        if not self.ack_test(ripmessage):
            return False
        #print 'ack test'
        if not self.verify_client_signature(ripmessage):
            print 'no'
            return False
        #print 'cert'
        print 'verify signature'
        self.client_third_handshake(ripmessage)
        return True

    def nonce_test(self, ripmessage, old_Nonce_1):

        nonce1 = old_Nonce_1
        nonce1_test = ripmessage.certificate[1]
        if not int(nonce1) == int(nonce1_test) - 1:
            return False
        #print 'nonce1 test: yes'
        return True

    def client_third_handshake(self, ripmessage):
        if not self.state == 'client_second_handshake':
            return False
        self.state = 'client_third_handshake'
        print self.state
        clienthandshake1_Nonce1 = self.Nonce_1
        if not self.nonce_test(ripmessage, clienthandshake1_Nonce1):
            return False
        #print 'True'
        #print self.state
        # if not self.state == 'SecondHandShake_success':
        #     return False
        self.acknowledgement_flag = True
        self.sequence_number = ripmessage.acknowledgement_number + 1
        self.acknowledgement_number = ripmessage.sequence_number + 1
        ripmessage.acknowledgement_flag = self.acknowledgement_flag
        ripmessage.acknowledgement_number = self.acknowledgement_number
        ripmessage.sequence_number = self.sequence_number
        cert3 = []
        Nonce3 = int(ripmessage.certificate[0]) + 1
        cert3.append(Nonce3)
        cert3.append(CertFactory.getPrivateKeyForAddr(self.transport.getHost().host))

        ripmessage.certificate = cert3
        #print 'cert:', ripmessage.certificate
        #print 'ThirdHandshake_success'
        self.transport.write(ripmessage.__serialize__())
        #self.state = 'ThirdHandshake_success'
        self.state = 'data transport'
        #print self.state
        self.makeHigherConnection(self.higherTransport)
        return True

    def client_handshake(self):
        #ripmessage = RipMessage()
        if not self.state == 'listen':
            return False
        self.client_first_handshake()


    def client_DataReceive(self, ripmessage):
        #data = ripmessage.data
        self.higherProtocol() and self.higherProtocol().dataReceived(ripmessage.data)
        return ripmessage.data
    def client_reconnect(self):
        pass
    def client_close(self):
        pass

    def connectionMade(self):
        #initialize
        self.higherTransport = RipTransport(self.transport, self)
        self.client_handshake()


    def dataReceived(self, data):
        #in case of more than one serialized data
        # #use buffer to store all the serialized data and then process every single ripmessage
        # self.buffer += data
        # try:
        #     ripmessage, bytesUsed = RipMessage.Deserialize(self.buffer)
        #     self.buffer = self.buffer[bytesUsed:]
        # except Exception, e:
        #     print "We had a client deserialization error", e
        #     return
        #print 'client data receive:', ripmessage.acknowledgement_flag
        #self.higherProtocol() and self.higherProtocol().dataReceived(ripmessage.data)
        #if buffer is not null, continue to store the buffer,
        #if buffer is null, the exit.
        #self.__buffer and self.dataReceived("")
        self.message_store.update(data)
        try:
            for message in self.message_store.iterateMessages():
                if self.state == 'client_first_handshake':
                    self.client_second_HandShake(message)
                elif self.state == 'data transport':
                    self.client_DataReceive(message)
                    # ripmessage, byteused = RipMessage.Deserialize(data)
                    # self.buffer = self.buffer[byteused:]
                    # self.message_store = self.message_store[byteused:]
        except Exception, e:
            #print "We had a server deserialization error", e
            return



class RipServerProtocol(StackingProtocolMixin, Protocol):
    def __init__(self):
        self.address = "20164.1.100.20"
        self.buffer = ""
        self.state = "listen"
        self.message_store = MessageStorage()

    def verify_certificate(self, ripmessage):
        #certificate chain;
        crypto = RipCrypto()

        root_cert = CertFactory.getRootCert()
        my_cert = CertFactory.getCertsForAddr(self.transport.getHost().host)[0]
        ca_cert = CertFactory.getCertsForAddr(self.transport.getHost().host)[1]
        if not crypto.verify_cert_chain(ca_cert, root_cert):
            print 'root cert fail'
            return False
        if not crypto.verify_cert_chain(my_cert, ca_cert):
            #print 'no'
            return False
        #print 'verify certificate: yes'
        return True


    def verify_server_signature(self, ripmessage):
        crypto = RipCrypto()
        #print 'sign:', ripmessage.signature
        #ca_cert = CertFactory.getCertsForAddr(self.transport.getHost().host)[1]
        peer_cert = self.peerCerts[0]
        if not crypto.verify_signature(ripmessage,peer_cert):
            #print 'func:signature fail'
            return False
        print 'verify signature: yes'
        return True

    def check_snn_flag(self, ripmessage):
        #print "snn:", ripmessage.sequence_number_notification_flag
        if ripmessage.sequence_number_notification_flag == False:
            return False
        return True

    def server_first_handshake(self, ripmessage):
        #print 'nonce:', ripmessage.certificate[0]
        #print 'signature:', ripmessage.signature
        self.peerCerts = ripmessage.certificate[1:]
        if not self.check_snn_flag(ripmessage):
            return False
        self.state = 'server_first_handshake'
        print self.state
        if not self.verify_server_signature(ripmessage):
            #print 'signature fail'
            return False
        print 'signature'
        if not self.verify_certificate(ripmessage):
            return False

        self.server_second_handshake(ripmessage)
        return True

    def server_second_handshake(self, ripmessage):
        #print '--------------------------'
        if not self.state == 'server_first_handshake':
            return False
        self.state = 'server_second_handshake'
        print self.state
        crypto = RipCrypto()
        #print self.state
        # if not self.state == 'FirstHandShake_success':
        #     return False
        #self.sequence_number = random.randint(0, 65536)
        self.acknowledgement_number = ripmessage.sequence_number + 1
        #print "server ack:", self.acknowledge
        self.sequence_number = 9000
        #print 'seq_num:', self.sequence_number
        ripmessage.sequence_number = self.sequence_number
        self.sessionID = ''
        ripmessage.sessionID = self.sessionID
        self.acknowledgement_flag = True
        ripmessage.acknowledgement_flag = self.acknowledgement_flag
        Nonce2 = random.randint(0, 65536)
        #print Nonce2
        cert2 = []
        #cert2[:] =
        cert2.append(Nonce2)
        #print cert2
        Nonce1 = ripmessage.certificate[0]
        #print type(Nonce1)
        #print Nonce1
        Nonce1 = int(Nonce1) + 1
        cert2.append(Nonce1)
        #print cert2
        cert2 += CertFactory.getCertsForAddr(self.transport.getHost().host)
        self.certsnew = cert2[2:]


        ripmessage.certificate = cert2

        signature = crypto.hash_data(ripmessage.__serialize__(),
                                     RSA.importKey(CertFactory.getPrivateKeyForAddr(self.transport.getHost().host)))
        # signature = crypto.hash_data(ripmessage.__serialize__(), CertFactory.privateKey)
        ripmessage.signature = signature
        #self.state = 'SecondHandShake_success'
        #print 'server_second_handshake send'
        #print 'server 3rd handshake success'
        #print ripmessage.certificate
        self.transport.write(ripmessage.__serialize__())
        return True

    def server_third_handshake(self):
        #print '--------------------------'
        if not self.state == 'server_second_handshake':
            return False
        self.state = 'server_third_handshake'
        print self.state
        self.state = 'data transport'
        self.makeHigherConnection(self.higherTransport)
        return True

    #three time handshake
    def server_handshake(self, ripmessage):
        if not self.state == 'listen':
            return False
        self.server_first_handshake(ripmessage)

    def ServerReconnect(self):
        pass

    def Serverclose(self):
        pass

    def ServerDataSend(self):
        pass

    def ServerDataReceive(self, ripmessage):
        #data = ripmessage.data
        # self.message_store.update(data)
        # for message in self.message_store.iterateMessages():
        self.higherProtocol() and self.higherProtocol().dataReceived(ripmessage.data)
        return ripmessage.data


    def connectionMade(self):
        self.higherTransport = RipTransport(self.transport, self)
        #self.server_handshake()


    def dataReceived(self, data):
        #print "buffer:", self.buffer
        #self.buffer += data
        #self.message_store += data
        self.message_store.update(data)
        try:
            for message in self.message_store.iterateMessages():
                if self.state == 'listen':
                    self.server_handshake(message)
                elif self.state == 'server_second_handshake':
                    self.server_third_handshake()
                elif self.state == 'data transport':
                    # print 'data transport'
                    self.ServerDataReceive(message)
                #ripmessage, byteused = RipMessage.Deserialize(data)
            #self.buffer = self.buffer[byteused:]
            #self.message_store = self.message_store[byteused:]
        except Exception, e:
           # print "We had a server deserialization error", e
            return
        #print "server certificate:", ripmessage.certificate
        #print "server seq_num:", ripmessage.sequence_number
        #print ripmessage.sequence_number
        #ripmessage.state = "ReadyForHandShake"
        #self.higherProtocol() and self.higherProtocol().dataReceived(ripmessage.data)
        #self.buffer and self.dataReceived("")


class ClientFactory(StackingFactoryMixin, Factory):
    protocol = RipClientProtocol

class ServerFactory(StackingFactoryMixin, Factory):
    protocol = RipServerProtocol

#client
ConnectFactory = ClientFactory
#server
ListenFactory = ServerFactory