#!/usr/bin/env python
#!/usr/bin/python
# -*- coding: utf-8 -*-
from twisted.internet.protocol import Protocol, Factory
from zope.interface.declarations import implements
from twisted.internet.interfaces import ITransport, IStreamServerEndpoint
from playground.network.message.ProtoBuilder import MessageDefinition
from playground.network.message.StandardMessageSpecifiers import STRING,UINT4,OPTIONAL,DEFAULT_VALUE,\
    LIST,BOOL1
from playground.network.common.Protocol import StackingTransport,\
    StackingProtocolMixin, StackingFactoryMixin
import random
from PassThrough import RipMessage, RipTransport, RipCrypto

class RipClientProtocol(StackingProtocolMixin, Protocol):
    def __init__(self):
        self.address = "20164.1.100.10"
        self.buffer = ""
        self.state = "ReadyForHandShake"

    #three time handshake

    def client_first_handshake(self):
        print 'client 1st state:', self.state
        if not self.state == 'ReadyForHandShake':
            return False
        crypto = RipCrypto()
        ripmessage = RipMessage()
        ripmessage.sequence_number = 100
        ripmessage.acknowledgement_number = 0
        #sign:the signature of the whole msg
        ripmessage.signature = ""
        #cert:1.Nonce1, 2.sender's certificate, 3.CA's certificate
        ripmessage.certificate = []
        ripmessage.session_id = ""
        ripmessage.acknowledgement_flag = False
        ripmessage.close_flag = False
        ripmessage.sequence_number_notification_flag = True
        ripmessage.reset_flag = False
        ripmessage.data = ""
        ripmessage.options = []

        #cert
        Nonce1 = random.randint(0, 65536)
        print 'Nonce1:', Nonce1
        #print 'type', type(ripmessage.certificate)
        certs = []
        certs.append(Nonce1)
        sender_cert = crypto.load_cert('jinglunliu_user_signed.cert')
        certs.append(sender_cert)
        CA_cert = crypto.load_cert('jinglunliu_signed.cert')
        certs.append(CA_cert)
        ripmessage.certificate=certs
        print 'cert:', ripmessage.certificate

        #signature
        signature = crypto.hash_data(ripmessage.__serialize__(), 'key.txt')
        ripmessage.signature = signature
        #print 'sign:', ripmessage.signature

        self.higherTransport.rip_write(ripmessage)
        #self.higherTransport.rip_write()
        self.state = 'FirstHandShake_success'
        return True

    def nonce_test(self, ripmessage):
        print 'Nonce test'
        nonce1 = ripmessage.certificate[0]
        nonce1_test = ripmessage.certificate[3]
        print 'Nonce1:', nonce1
        print 'Nonce1_test', nonce1_test
        if not int(nonce1) == int(nonce1_test) - 1:
            return False
        print 'nonce1 test: yes'
        return True
    def ack_test(self, ripmessage):
        pass
    def verify_signature(self, ripmessage, rawkey):
        crypto = RipCrypto()
        #print 'sign:', ripmessage.signature
        if not crypto.verify_signature(ripmessage,rawkey):
            print 'func:signature fail'
            return False
        print 'verify signature: yes'
        return True
    def verify_certificate(self, ripmessage):
        #certificate chain;
        crypto = RipCrypto()
        if not crypto.verify_cert_chain(ripmessage):
            print 'no'
            return False
        print 'verify certificate: yes'
        return True


    def client_second_HandShake(self, ripmessage):
        if self.state == '3rd HandShake_success':
            return False
        print 'state:', self.state
        #print 'client 2 handshake:', ripmessage.acknowledgement_flag
        if not ripmessage.acknowledgement_flag:
            return False
        print 'ack_flag: true'
        if not self.nonce_test(ripmessage):
            return False
        print 'nonce_test success'
        if not self.verify_certificate(ripmessage):
            return False
        print 'verify certificate'
        # # if not self.verify_signature(ripmessage, 'jinglunliu_signed.cert'):
        # #     #print 'signature fail'
        # #     #return False
        # print 'verify signature'
        self.state == 'SecondHandShake_success'
        print 'SecondHandShake_success'
        print '--------------------------'
        self.client_third_handshake(ripmessage)
        return True

    def client_third_handshake(self, ripmessage):
        # if not self.state == 'SecondHandShake_success':
        #     return False
        self.acknowledgement_flag = True
        self.sequence_number = ripmessage.acknowledgement_number + 1
        self.acknowledgement_number = ripmessage.sequence_number + 1
        ripmessage.acknowledgement_flag = self.acknowledgement_flag
        ripmessage.acknowledgement_number = self.acknowledgement_number
        ripmessage.sequence_number = self.sequence_number
        cert3 = []
        cert3[:] = ripmessage.certificate[:]
        cert3.append(int(ripmessage.certificate[4])+1)
        ripmessage.certificate = cert3
        print 'ThirdHandshake_success'
        self.higherTransport.rip_write(ripmessage)
        #self.state = 'ThirdHandshake_success'
        self.state = '3rd HandShake_success'
        return True
        #return True

    def client_handshake(self):
        ripmessage = RipMessage()
        self.state == 'ReadyForHandShake'
        self.client_first_handshake()
        if self.state == 'ThirdHandShake_success':
            print 'make higher connection'
            self.makeHigherConnection(self.higherTransport)


    def client_reconnect(self):
        pass
    def client_close(self):
        pass

    def connectionMade(self):
        self.higherTransport = RipTransport(self.transport)
        self.client_handshake()

    def client_data_send(self,data):
        self.higherTransport.write(data)

    def dataReceived(self, data):
        #in case of more than one serialized data
        #use buffer to store all the serialized data and then process every single ripmessage
        self.buffer += data
        try:
            ripmessage, bytesUsed = RipMessage.Deserialize(self.buffer)
            self.buffer = self.buffer[bytesUsed:]
        except Exception, e:
            print "We had a client deserialization error", e
            return
        #print 'client data receive:', ripmessage.acknowledgement_flag
        #self.higherProtocol() and self.higherProtocol().dataReceived(ripmessage.data)
        #if buffer is not null, continue to store the buffer,
        #if buffer is null, the exit.
        #self.__buffer and self.dataReceived("")
        self.client_second_HandShake(ripmessage)
