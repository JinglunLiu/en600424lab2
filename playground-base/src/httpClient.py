#!/usr/bin/env python
#!/usr/bin/python
# -*- coding: utf-8 -*-
""" 

"""

from twisted.internet.protocol import Protocol,ClientFactory
from twisted.internet import reactor
import requests
import PassThrough2
#import ripClient
from playground.twisted.endpoints import GateClientEndpoint

class HttpClient(Protocol):


    def __init__(self):
        pass


    def connectionMade(self):
        print 'client connect'
        str = 'hello'
        self.transport.write(str)
        self.transport.write(str)
        #print 'client send', str

    def dataReceived(self, data):
        print ('Server said:', data)

class httpClientFactory(ClientFactory):

    def __init__(self):
        pass

    def startedConnecting(self, connector):
        print 'started connecting'

    def buildProtocol(self, addr):
        print 'connected'
        return HttpClient()

    def clientConnectionLost(self, connector, reason):
        print 'Lost connection. Reason:', reason
        print 'prepare to reconnect'
        connector.connect()

    def clientConnectionFailed(self, connector, reason):
        print 'Connection failed. Reason', reason

class Helper:

    def __init__(self):
        pass

def main():
    #f = HttpClientFactory()
    #endpoint = GateClientEndpoint(reactor, '20164.0.0.1', 8101, "127.0.0.1", 19090)
    echoClientEndpoint = GateClientEndpoint.CreateFromConfig(reactor, '20164.0.0.1', 8101, networkStack=PassThrough2)
    echoClientEndpoint.connect(httpClientFactory())
    reactor.run()

if __name__ == '__main__':
	main()
