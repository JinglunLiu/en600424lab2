#!/usr/bin/env python
#!/usr/bin/python
# -*- coding: utf-8 -*-
from twisted.internet.protocol import Protocol, Factory
from zope.interface.declarations import implements
from twisted.internet.interfaces import ITransport, IStreamServerEndpoint
from playground.network.message.ProtoBuilder import MessageDefinition
from playground.network.message.StandardMessageSpecifiers import STRING,UINT4,OPTIONAL,DEFAULT_VALUE,\
    LIST,BOOL1
from playground.network.common.Protocol import StackingTransport,\
    StackingProtocolMixin, StackingFactoryMixin
import random
from PassThrough import RipMessage, RipTransport, RipCrypto
class RipServerProtocol(StackingProtocolMixin, Protocol):
    def __init__(self):
        self.address = "20164.1.100.20"
        self.buffer = ""
        self.state = "ReadyForHandShake"

    def verify_certificate(self, ripmessage):
        #certificate chain;
        crypto = RipCrypto()
        if not crypto.verify_cert_chain(ripmessage):
            print 'no'
            return False
        print 'verify certificate: yes'
        return True


    def verify_signature(self, ripmessage):
        crypto = RipCrypto()
        #print 'sign:', ripmessage.signature
        if not crypto.verify_signature(ripmessage,'jinglunliu_signed.cert'):
            print 'func:signature fail'
            return False
        print 'verify signature: yes'
        return True

    def check_snn_flag(self, ripmessage):
        #print "snn:", ripmessage.sequence_number_notification_flag
        if ripmessage.sequence_number_notification_flag == False:
            return False
        return True

    def server_first_handshake(self, ripmessage):
        print 'server 1st state:', self.state
        if not self.state == 'ReadyForHandShake':
            return False
        #print 'nonce:', ripmessage.certificate[0]
        #print 'signature:', ripmessage.signature
        if not self.verify_signature(ripmessage):
            #print 'signature fail'
            return False
        if not self.verify_certificate(ripmessage):
            return False
        if not self.check_snn_flag(ripmessage):
            return False
        self.state = 'FirstHandShake_success'
        self.server_second_handshake(ripmessage)
        return True

    def server_second_handshake(self, ripmessage):
        print 'server 2nd state:', self.state
        # if not self.state == 'FirstHandShake_success':
        #     return False
        #self.sequence_number = random.randint(0, 65536)
        self.acknowledgement_number = ripmessage.sequence_number + 1

        #print "server ack:", self.acknowledge
        self.sequence_number = 9000
        print 'seq_num:', self.sequence_number
        ripmessage.sequence_number = self.sequence_number
        self.acknowledgement_flag = True
        ripmessage.acknowledgement_flag = self.acknowledgement_flag
        Nonce2 = random.randint(0, 65536)
        cert2 = []
        cert2[:] = ripmessage.certificate[:]
        #print 'cert2:', cert2
        #print type(ripmessage.certificate[0])
        Nonce1 = ripmessage.certificate[0]
        cert2.append(int(Nonce1)+1)
        cert2.append(Nonce2)
        ripmessage.certificate = cert2
        self.state = 'SecondHandShake_success'
        print 'server_second_handshake send'
        print 'server 3rd handshake success'
        #print ripmessage.certificate
        self.higherTransport.rip_write(ripmessage)

        return True

    # def server_third_handshake(self, ripmessage):
    #     print 'server 3rd state:', self.state
    #     if not self.state == 'SecondHandShake_success':
    #         return False
    #     self.state = 'ThirdHandshake_success'
    #     return True

    #three time handshake
    def server_handshake(self, ripmessage):
        self.state == 'ReadyForHandShake'
        self.server_first_handshake(ripmessage)
        print 'server 1st handshake success'
        print '---------------------------------'
        self.server_second_handshake(ripmessage)
        if self.state == 'ThirdHandShake_success':
            self.makeHigherConnection(self.higherTransport)



    def ServerReconnect(self):
        pass

    def Serverclose(self):
        pass

    def ServerDataSend(self):
        pass

    def ServerDataReceive(self):
        pass

    def connectionMade(self):
        self.higherTransport = RipTransport(self.transport)
        #self.server_handshake()


    def dataReceived(self, data):
        print 'receiver receives the data from sender'
        #print "buffer:", self.buffer
        self.buffer += data
        try:
            ripmessage, byteused = RipMessage.Deserialize(data)
            self.buffer = self.buffer[byteused:]
        except Exception, e:
            print "We had a server deserialization error", e
            return
        #print "server certificate:", ripmessage.certificate
        #print "server seq_num:", ripmessage.sequence_number
        #print ripmessage.sequence_number
        ripmessage.state = "ReadyForHandShake"
        self.server_handshake(ripmessage)


        #self.higherProtocol() and self.higherProtocol().dataReceived(ripmessage.data)
        #self.buffer and self.dataReceived("")
