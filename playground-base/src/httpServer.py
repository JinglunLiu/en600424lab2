#!/usr/bin/env python
#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
"""

from twisted.internet.protocol import Protocol
from twisted.internet import reactor
from twisted.internet.protocol import ServerFactory
#import SampleStack
import PassThrough2
#import ripServer
from playground.twisted.endpoints import GateServerEndpoint

class httpServer(Protocol):
    
    def __init__(self):
        pass


    def connectionMade(self):
        print "hello connection made"

    def dataReceived(self, data):
        print 'http server received:', data
        self.transport.write(data)
        print 'http server write:', data
        

def main():
    factory = ServerFactory()
    factory.protocol = httpServer
    # use 8101 in case of port used
    # endpoint = GateServerEndpoint(reactor, 8101, "127.0.0.1", 19090)
    echoServerEndpoint = GateServerEndpoint.CreateFromConfig(reactor, 8101, networkStack=PassThrough2)
    echoServerEndpoint.listen(factory)
    reactor.run()

if __name__ == '__main__':
	main()
