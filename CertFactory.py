# File CertFactory.py
privateKey = "/Users/liujinglun/Desktop/Course/Network_Security/Lab2/playground-base/src/key.txt"
my_cert = "/Users/liujinglun/Desktop/Course/Network_Security/Lab2/playground-base/src/jinglunliu_user_signed.cert"
CA_cert = "/Users/liujinglun/Desktop/Course/Network_Security/Lab2/playground-base/src/jinglunliu_signed.cert"
root_cert = "/Users/liujinglun/Desktop/Course/Network_Security/Lab2/playground-base/src/20164_signed.cert"
def getCertsForAddr(addr):
  chain = []
  with open(my_cert) as f:
    chain.append(f.read())
  with open(CA_cert) as f:
    chain.append(f.read())
  return chain

def getPrivateKeyForAddr(addr):
  # ignoring addr, always return the same thing
  with open(privateKey) as f:
    return f.read()

def getRootCert():
  with open(root_cert) as f:
    return f.read()
